class Sale < ActiveRecord::Base
  belongs_to :customer

  max_paginates_per 50

  def amount_with_tax
    self.amount + self.tax
  end
end
